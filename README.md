# reporter-api
## what is Reporter-Api ?
web page that reports back-end server name and IP address.
## web app tech details
reporter.py is a python Flask application.
### Routes
- /
- /IPAddress
- /Servername

### Authentication
HTTP basic authentication is incorporated within a python dictionary

Current MVP Credentials: "username":"password"
upon visiting the routes, you will be prompted for credetials as these endpoints are protected via HTTP basic auth.
### why python Flask
the choice to go with python flask was an easy one as opposed to other alternatives like:
- PHP script
- Laravel/Symfony

while incredible choices, the above technologies come with a lot of packages that we don't need for this simple API. 
with Python Flask on the otherhand I was able to build an efficient flask app with authentication.

## Production Stack

gunicorn / nginx / Ubuntu 16.04

the flask application is great for development but not great in the production enviroment as a web server comes with its own challenges.
And that's precisely why Gunicorn[https://gunicorn.org/] in combination with Nginx as a reverse proxy seems like a more secure way to go.

## Automated Deployment


