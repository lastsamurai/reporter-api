import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="reporter-pkg-lastsamurai", 
    version="0.0.1",
    author="Hadi Mir",
    author_email="caffotinated@gmail.com",
    description="simple web app reporting ip address and servername",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/lastsamurai/reporter-api",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
